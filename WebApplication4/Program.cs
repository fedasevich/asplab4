using WebApplication4;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/Library", () => "Welcome in Library API!");

builder.Configuration.AddJsonFile("books.json", optional: false, reloadOnChange: true);

app.MapGet("/Library/Books", async (context) =>
{
    var books = builder.Configuration.GetSection("books").Get<List<Book>>();

    var response = context.Response;
    response.ContentType = "text/html; charset=utf-8";

    await response.WriteAsync("<h2>List of Books</h2>");

    await response.WriteAsync("<ul>");

    foreach (var book in books)
    {
        await response.WriteAsync($"<li>{book.Name} by {book.Author} - genre: {book.Genre}, length: {book.Length} pages</li>");
    }
    
    await response.WriteAsync("</ul>");
});

builder.Configuration.AddJsonFile("users.json", optional: false, reloadOnChange: true);

app.MapGet("/Library/Profile/{id:int:range(1, 5)?}", (int? id) =>
{
    var users = builder.Configuration.GetSection("users").Get<List<User>>();

    if (!id.HasValue)
    {
        return users[0];
    }

    return users[id.Value];
    
});

app.Run();
