﻿namespace WebApplication4
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public List<int> TakenBooks { get; set; }

    };
}